# PI_MathGames

Full Stack Application about virtual board games

# The Team
    Diogo Carvalho	  - Team Manager
 	Diogo Cunha       - UX Designer
 	Pedro Amaral	  - Architect
 	Pedro Santos	  - QA Engineer
	Rafael Batista	  - DevOps Master
	Ricardo Cruz	  - Product Owner

# Deployed Website

http://138.68.191.32/

# JIRA Backlog

https://pedromiglou.atlassian.net/jira/software/projects/MAT/boards/3

# Promotional Website

http://xcoa.av.it.pt/~pi202021g07/
